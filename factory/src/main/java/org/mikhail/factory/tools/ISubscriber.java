package org.mikhail.factory.tools;

public interface ISubscriber {
    void reactOnNotify();
}