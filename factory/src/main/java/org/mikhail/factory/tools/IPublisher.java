package org.mikhail.factory.tools;

public interface IPublisher {
    void addSubscriber(ISubscriber subscriber);

    boolean removeSubscriber(ISubscriber subscriber);

    void publishNotify();
}