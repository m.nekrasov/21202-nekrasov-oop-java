package org.mikhail.factory.controller.commands;

import org.mikhail.factory.exceptions.CommandException;
import org.mikhail.factory.exceptions.InvalidArgumentException;
import org.mikhail.factory.model.FactoryManager;

public class SetBodySupplierDelayCommand implements ICommand {
    @Override
    public void execute(FactoryManager factoryManager, Long... arguments) throws CommandException {
        if (arguments == null) {
            throw new InvalidArgumentException("Too few arguments");
        }

        factoryManager.getEmployeesManager().setBodySupplierDelay(arguments[0]);
    }
}
