package org.mikhail.factory.controller;

import org.mikhail.factory.controller.commands.ICommand;
import org.mikhail.factory.exceptions.CommandException;
import org.mikhail.factory.exceptions.ControllerConfigurationException;
import org.mikhail.factory.exceptions.InvalidCommandException;
import org.mikhail.factory.model.FactoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

public class BasicController extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(BasicController.class);

    private final ControllerConfiguration controllerConfiguration;

    public BasicController(FactoryManager factoryManager) throws ControllerConfigurationException {
        super(factoryManager);

        controllerConfiguration = new ControllerConfiguration();
    }

    @Override
    public void processCommand(String commandName, Long... arguments) {
        try {
            executeCommand(commandName, arguments);
        } catch (CommandException exception) {
            logger.warn(exception.getMessage(), exception);
        }
    }

    @Override
    protected ICommand createCommand(String commandName) throws InvalidCommandException {
        try {
            return (ICommand) Class
                    .forName(controllerConfiguration.getCommandClassPath(commandName))
                    .getDeclaredConstructor()
                    .newInstance();
        } catch (ClassNotFoundException exception) {
            throw new InvalidCommandException("Failed to find command \"" + commandName + "\"", exception);
        } catch (
                NoSuchMethodException
                | InstantiationException
                | IllegalAccessException
                | InvocationTargetException exception
        ) {
            throw new InvalidCommandException("Failed to create command \"" + commandName + "\"", exception);
        }
    }
}
