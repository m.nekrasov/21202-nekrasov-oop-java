package org.mikhail.factory.controller.commands;

import org.mikhail.factory.exceptions.CommandException;
import org.mikhail.factory.model.FactoryManager;

public interface ICommand {
    void execute(FactoryManager factoryManager, Long... arguments) throws CommandException;
}
