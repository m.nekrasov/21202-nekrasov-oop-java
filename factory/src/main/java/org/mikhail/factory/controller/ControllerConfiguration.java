package org.mikhail.factory.controller;

import org.mikhail.factory.exceptions.ControllerConfigurationException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ControllerConfiguration {
    private final Properties properties = new Properties();

    public ControllerConfiguration() throws ControllerConfigurationException {
        try (InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream("controllerConfiguration.properties")) {
            properties.load(inputStream);
        } catch (IOException exception) {
            throw new ControllerConfigurationException("Failed to load controller configuration");
        }
    }

    public String getCommandClassPath(String commandName) {
        return properties.getProperty(commandName);
    }
}