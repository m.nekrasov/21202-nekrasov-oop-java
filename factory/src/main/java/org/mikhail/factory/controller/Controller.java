package org.mikhail.factory.controller;

import org.mikhail.factory.controller.commands.ICommand;
import org.mikhail.factory.exceptions.CommandException;
import org.mikhail.factory.exceptions.InvalidCommandException;
import org.mikhail.factory.model.FactoryManager;

public abstract class Controller {
    private final FactoryManager factoryManager;

    protected Controller(FactoryManager factoryManager) {
        this.factoryManager = factoryManager;
    }

    protected void executeCommand(String commandName, Long... arguments) throws CommandException {
        try {
            ICommand command = createCommand(commandName);
            command.execute(factoryManager, arguments);
        } catch (InvalidCommandException exception) {
            throw new CommandException("Failed to create command");
        }
    }

    public abstract void processCommand(String commandName, Long... arguments);

    protected abstract ICommand createCommand(String commandName) throws InvalidCommandException;
}