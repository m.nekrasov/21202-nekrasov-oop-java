package org.mikhail.factory;

import org.mikhail.factory.controller.BasicController;
import org.mikhail.factory.controller.Controller;
import org.mikhail.factory.exceptions.ControllerConfigurationException;
import org.mikhail.factory.exceptions.FactoryConfigurationException;
import org.mikhail.factory.model.FactoryConfiguration;
import org.mikhail.factory.model.FactoryManager;
import org.mikhail.factory.model.employees.EmployeesManager;
import org.mikhail.factory.model.employees.Supplier;
import org.mikhail.factory.model.employees.unions.AccessorySuppliersUnion;
import org.mikhail.factory.model.employees.unions.DealersUnion;
import org.mikhail.factory.model.employees.unions.WorkersUnion;
import org.mikhail.factory.model.productsFactory.AccessoryCreator;
import org.mikhail.factory.model.productsFactory.BodyCreator;
import org.mikhail.factory.model.productsFactory.CarCreator;
import org.mikhail.factory.model.productsFactory.EngineCreator;
import org.mikhail.factory.model.productsFactory.products.Body;
import org.mikhail.factory.model.productsFactory.products.Engine;
import org.mikhail.factory.model.storage.StorageManager;
import org.mikhail.factory.threadpool.ITask;
import org.mikhail.factory.threadpool.ThreadPool;
import org.mikhail.factory.view.MainFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        FactoryConfiguration factoryConfiguration;
        try {
            factoryConfiguration = new FactoryConfiguration();
        } catch (FactoryConfigurationException exception) {
            logger.error(exception.getMessage(), exception);
            return;
        }

        StorageManager storageManager = new StorageManager(
                factoryConfiguration.getBodyStorageSize(),
                factoryConfiguration.getEngineStorageSize(),
                factoryConfiguration.getAccessoryStorageSize(),
                factoryConfiguration.getCarStorageSize()
        );

        BodyCreator bodyCreator = new BodyCreator();
        EngineCreator engineCreator = new EngineCreator();
        AccessoryCreator accessoryCreator = new AccessoryCreator();
        CarCreator carCreator = new CarCreator();

        AccessorySuppliersUnion accessorySuppliersUnion = new AccessorySuppliersUnion(
                factoryConfiguration.getAccessorySuppliers(),
                1000,
                storageManager,
                accessoryCreator
        );
        DealersUnion dealersUnion = new DealersUnion(
                factoryConfiguration.getDealers(),
                1000,
                storageManager
        );
        WorkersUnion workersUnion = new WorkersUnion(
                factoryConfiguration.getWorkers(),
                storageManager,
                carCreator
        );

        Supplier<Body> bodySupplier = new Supplier<>(
                "Body supplier",
                storageManager.getBodyStorage(),
                bodyCreator,
                1000
        );
        Supplier<Engine> engineSupplier = new Supplier<>(
                "Engine supplier",
                storageManager.getEngineStorage(),
                engineCreator,
                1000
        );

        EmployeesManager employeesManager = new EmployeesManager(
                accessorySuppliersUnion,
                dealersUnion,
                workersUnion,
                bodySupplier,
                engineSupplier
        );

        FactoryManager factoryManager = new FactoryManager(storageManager, employeesManager);

        ThreadPool threadPool = new ThreadPool(employeesManager.getEmployeesCount());
        for (ITask employee : employeesManager.getEmployees()) {
            threadPool.addTask(employee);
        }

        Controller controller;
        try {
            controller = new BasicController(factoryManager);
        } catch (ControllerConfigurationException exception) {
            logger.error(exception.getMessage(), exception);
            return;
        }

        MainFrame mainFrame = new MainFrame(factoryManager, controller, threadPool);
        mainFrame.setVisible(true);

        // Debug:
        // while (true) {
        //     mainFrame.reactOnNotify();
        // }
    }
}