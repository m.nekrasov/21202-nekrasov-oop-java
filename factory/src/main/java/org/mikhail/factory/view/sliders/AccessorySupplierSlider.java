package org.mikhail.factory.view.sliders;

import org.mikhail.factory.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class AccessorySupplierSlider extends JPanel {
    private final JSlider slider = new JSlider(0, 2000, 1000);
    private final JLabel label = new JLabel("Accessory supplier delay: " + slider.getValue() + " ms");

    public AccessorySupplierSlider(Controller controller) {
        setBackground(Color.WHITE);

        slider.setMajorTickSpacing(1);
        slider.setBackground(Color.WHITE);
        slider.setPaintTicks(true);
        slider.addChangeListener(e -> {
            controller.processCommand("SetAccessorySupplierDelayCommand", (long) slider.getValue());
            label.setText("Accessory suppliers delay: " + slider.getValue() + " ms");
        });

        add(slider);
        add(label);
    }
}
