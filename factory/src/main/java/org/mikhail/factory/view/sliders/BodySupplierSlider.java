package org.mikhail.factory.view.sliders;

import org.mikhail.factory.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class BodySupplierSlider extends JPanel {
    private final JSlider slider = new JSlider(0, 2000, 1000);
    private final JLabel label = new JLabel("Body supplier delay: " + slider.getValue() + " ms");

    public BodySupplierSlider(Controller controller) {
        setBackground(Color.WHITE);

        slider.setMajorTickSpacing(1);
        slider.setBackground(Color.WHITE);
        slider.setPaintTicks(true);
        slider.addChangeListener(e -> {
            controller.processCommand("SetBodySupplierDelayCommand", (long) slider.getValue());
            label.setText("Body supplier delay: " + slider.getValue() + " ms");
        });

        add(slider);
        add(label);
    }
}
