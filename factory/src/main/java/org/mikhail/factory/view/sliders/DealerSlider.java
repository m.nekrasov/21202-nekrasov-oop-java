package org.mikhail.factory.view.sliders;

import org.mikhail.factory.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class DealerSlider extends JPanel {
    private final JSlider slider = new JSlider(0, 2000, 1000);
    private final JLabel label = new JLabel("Dealer delay " + slider.getValue() + " ms");

    public DealerSlider(Controller controller) {
        setBackground(Color.WHITE);

        slider.setMajorTickSpacing(1);
        slider.setBackground(Color.WHITE);
        slider.setPaintTicks(true);
        slider.addChangeListener(e -> {
            controller.processCommand("SetDealerDelayCommand", (long) slider.getValue());
            label.setText("Dealer delay " + slider.getValue() + " ms");
        });

        add(slider);
        add(label);
    }
}
