package org.mikhail.factory.view.sliders;

import org.mikhail.factory.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class Sliders extends JPanel {
    public Sliders(Controller controller, Rectangle bounds) {
        setBounds(bounds);
        setBackground(Color.WHITE);

        add(new BodySupplierSlider(controller));
        add(new EngineSupplierSlider(controller));
        add(new AccessorySupplierSlider(controller));
        add(new DealerSlider(controller));
    }
}
