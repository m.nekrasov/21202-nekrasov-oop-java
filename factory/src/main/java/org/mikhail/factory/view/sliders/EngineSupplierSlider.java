package org.mikhail.factory.view.sliders;

import org.mikhail.factory.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class EngineSupplierSlider extends JPanel {
    private final JSlider slider = new JSlider(0, 2000, 1000);
    private final JLabel label = new JLabel("Engine suppler delay: " + slider.getValue() + " ms");

    public EngineSupplierSlider(Controller controller) {
        setBackground(Color.WHITE);

        slider.setMajorTickSpacing(1);
        slider.setBackground(Color.WHITE);
        slider.setPaintTicks(true);
        slider.addChangeListener(e -> {
            controller.processCommand("SetEngineSupplierDelayCommand", (long) slider.getValue());
            label.setText("Engine suppler delay: " + slider.getValue() + " ms");
        });

        add(slider);
        add(label);
    }
}
