package org.mikhail.factory.view;

import org.mikhail.factory.model.storage.StorageManager;
import org.mikhail.factory.threadpool.PooledThread;
import org.mikhail.factory.threadpool.ThreadPool;
import org.mikhail.factory.tools.ISubscriber;

import javax.swing.*;
import java.awt.*;

public class FactoryInfoPanel extends JPanel implements ISubscriber {
    private final Font font = new Font(Font.MONOSPACED, Font.BOLD, 12);
    private final StorageManager storageManager;
    private final ThreadPool threadPool;

    public FactoryInfoPanel(StorageManager storageManager, Rectangle bounds, ThreadPool threadPool) {
        setBounds(bounds);
        setBackground(Color.WHITE);

        this.storageManager = storageManager;
        this.threadPool = threadPool;
        storageManager.addSubscriber(this);
        repaint();
    }

    @Override
    public void reactOnNotify() {
        repaint();
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        drawInfo(graphics);
    }

    private void drawInfo(Graphics graphics) {
        graphics.setFont(font);
        graphics.setColor(new Color(0, 0, 0));

        graphics.drawString(String.format(
                "Body storage workload: %d | Bodies produced: %d",
                storageManager.getBodyStorage().getCurrentWorkload(),
                storageManager.getBodyStorage().getProduced()
        ), 0, 50);

        graphics.drawString(String.format(
                "Engine storage workload: %d | Engines produced: %d",
                storageManager.getEngineStorage().getCurrentWorkload(),
                storageManager.getEngineStorage().getProduced()
        ), 0, 70);

        graphics.drawString(String.format(
                "Accessory storage workload: %d | Accessories produced: %d",
                storageManager.getAccessoryStorage().getCurrentWorkload(),
                storageManager.getAccessoryStorage().getProduced()
        ), 0, 90);

        graphics.drawString(String.format(
                "Cars storage workload: %d | Cars produced: %d | Cars sold: %d",
                storageManager.getCarStorage().getCurrentWorkload(),
                storageManager.getCarStorage().getProduced(),
                storageManager.getCarStorage().getProduced() - storageManager.getCarStorage().getCurrentWorkload()
        ), 0, 110);

        int offset = 0;
        for (PooledThread pooledThread : threadPool.getAvailableThreads()) {
            graphics.drawString(String.format(
                    "Thread \"%s\" has task \"%s\" (it is %s)",
                    pooledThread.getName(),
                    pooledThread.getThreadTask().getName(),
                    (pooledThread.getState() == Thread.State.WAITING)
                            ? "waiting"
                            : "working"
            ), 450, 50 + offset++ * 12);
        }
    }
}