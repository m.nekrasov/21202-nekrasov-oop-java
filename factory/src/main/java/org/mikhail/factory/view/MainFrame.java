package org.mikhail.factory.view;

import org.mikhail.factory.controller.Controller;
import org.mikhail.factory.model.FactoryManager;
import org.mikhail.factory.threadpool.ThreadPool;
import org.mikhail.factory.tools.ISubscriber;
import org.mikhail.factory.view.sliders.Sliders;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame implements ISubscriber {
    private final FactoryManager factoryManager;
    private final Controller controller;
    private final ThreadPool threadPool;
    private final FactoryInfoPanel factoryInfoPanel;

    public MainFrame(FactoryManager factoryManager, Controller controller, ThreadPool threadPool) {
        this.factoryManager = factoryManager;
        this.controller = controller;
        this.threadPool = threadPool;

        factoryInfoPanel = new FactoryInfoPanel(factoryManager.getStorageManager(), new Rectangle(0, 0, 600, 300), threadPool);

        initUi();
    }

    @Override
    public void reactOnNotify() {
        factoryInfoPanel.reactOnNotify();
    }

    private void initUi() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(1000, 800);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        setFocusable(true);
        setResizable(false);

        add(factoryInfoPanel);
        add(new Sliders(controller, new Rectangle(0, 0, 600, 300)));
    }
}