package org.mikhail.factory.exceptions;

public class InvalidCommandException extends ControllerException {
    public InvalidCommandException(String message) {
        super(message);
    }

    public InvalidCommandException(String message, Throwable cause) {
        super(message, cause);
    }
}