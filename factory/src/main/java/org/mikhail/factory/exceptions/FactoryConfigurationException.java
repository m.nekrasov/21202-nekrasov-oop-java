package org.mikhail.factory.exceptions;

public class FactoryConfigurationException extends ConfigurationException {
    public FactoryConfigurationException(String message) {
        super(message);
    }

    public FactoryConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
