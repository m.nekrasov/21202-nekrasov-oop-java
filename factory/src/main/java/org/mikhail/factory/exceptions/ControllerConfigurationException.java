package org.mikhail.factory.exceptions;

public class ControllerConfigurationException extends ConfigurationException {
    public ControllerConfigurationException(String message) {
        super(message);
    }

    public ControllerConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
