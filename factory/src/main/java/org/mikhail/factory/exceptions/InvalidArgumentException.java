package org.mikhail.factory.exceptions;

public class InvalidArgumentException extends CommandException {
    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }
}
