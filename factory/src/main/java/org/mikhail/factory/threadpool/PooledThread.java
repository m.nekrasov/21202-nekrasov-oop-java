package org.mikhail.factory.threadpool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PooledThread extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(PooledThread.class);
    private final List<ITask> taskQueue;
    private ITask threadTask;

    public PooledThread(String name, List<ITask> taskQueue) {
        super(name);
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (taskQueue) {
                if (taskQueue.isEmpty()) {
                    try {
                        taskQueue.wait();
                    } catch (InterruptedException exception) {
                        logger.warn("Thread " + getName() + " was interrupted", exception);
                    }

                    continue;
                } else {
                    threadTask = taskQueue.remove(0);
                }
            }

            try {
                threadTask.performWork();
            } catch (InterruptedException exception) {
                logger.warn(exception.getMessage(), exception);
            }
        }
    }

    public ITask getThreadTask() {
        return threadTask;
    }
}
