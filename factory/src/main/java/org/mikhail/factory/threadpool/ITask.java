package org.mikhail.factory.threadpool;

public interface ITask {
    String getName();

    void performWork() throws InterruptedException;
}
