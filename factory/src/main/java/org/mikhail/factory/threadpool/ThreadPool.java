package org.mikhail.factory.threadpool;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ThreadPool {
    private final List<ITask> taskQueue = new LinkedList<>();
    private final Set<PooledThread> availableThreads = new HashSet<>();

    public ThreadPool(int threadsCount) {
        for (int i = 0; i < threadsCount; ++i) {
            availableThreads.add(new PooledThread("Performer " + i, taskQueue));
        }

        for (PooledThread availableThread : availableThreads) {
            availableThread.start();
        }
    }

    public void addTask(ITask task) {
        synchronized (taskQueue) {
            taskQueue.add(task);
            taskQueue.notify();
        }
    }

    public List<ITask> getTaskQueue() {
        return taskQueue;
    }

    public Set<PooledThread> getAvailableThreads() {
        return availableThreads;
    }
}
