package org.mikhail.factory.model.productsFactory;

import org.mikhail.factory.model.productsFactory.products.Product;

public abstract class ProductCreator<ProductType extends Product> {
    protected SerialNumberGenerator serialNumberGenerator;

    public ProductCreator(SerialNumberGenerator serialNumberGenerator) {
        this.serialNumberGenerator = serialNumberGenerator;
    }

    public abstract ProductType createProduct(Product... products);
}