package org.mikhail.factory.model.employees.unions;

import org.mikhail.factory.model.employees.Dealer;
import org.mikhail.factory.model.storage.StorageManager;

import java.util.ArrayList;
import java.util.List;

public class DealersUnion {
    private final List<Dealer> dealersUnion = new ArrayList<>();

    public DealersUnion(int dealersCount, long delay, StorageManager storageManager) {
        inviteDealers(dealersCount, delay, storageManager);
    }

    private void inviteDealers(int dealersCount, long delay, StorageManager storageManager) {
        for (int i = 0; i < dealersCount; ++i) {
            dealersUnion.add(new Dealer("Dealer " + i, storageManager, delay));
        }
    }

    public List<Dealer> getDealersUnion() {
        return dealersUnion;
    }

    public void setDelay(long delay) {
        for (Dealer dealer : dealersUnion) {
            dealer.setDelay(delay);
        }
    }
}
