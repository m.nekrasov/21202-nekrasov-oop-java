package org.mikhail.factory.model.employees.unions;

import org.mikhail.factory.model.employees.Worker;
import org.mikhail.factory.model.productsFactory.CarCreator;
import org.mikhail.factory.model.storage.StorageManager;

import java.util.ArrayList;
import java.util.List;

public class WorkersUnion {
    private final List<Worker> workersUnion = new ArrayList<>();

    public WorkersUnion(int workersCount, StorageManager storageManager, CarCreator carCreator) {
        inviteWorkers(workersCount, storageManager, carCreator);
    }

    private void inviteWorkers(int workersCount, StorageManager storageManager, CarCreator carCreator) {
        for (int i = 0; i < workersCount; ++i) {
            workersUnion.add(new Worker("Worker " + i, storageManager, carCreator));
        }
    }

    public List<Worker> getWorkersUnion() {
        return workersUnion;
    }
}
