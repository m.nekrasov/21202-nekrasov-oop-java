package org.mikhail.factory.model.storage;

import org.mikhail.factory.model.productsFactory.products.Accessory;
import org.mikhail.factory.model.productsFactory.products.Body;
import org.mikhail.factory.model.productsFactory.products.Car;
import org.mikhail.factory.model.productsFactory.products.Engine;
import org.mikhail.factory.tools.ISubscriber;
import org.mikhail.factory.tools.Publisher;

public class StorageManager extends Publisher implements ISubscriber {
    private final Storage<Body> bodyStorage;
    private final Storage<Engine> engineStorage;
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Car> carStorage;

    public StorageManager(int bodyStorageSize, int engineStorageSize, int accessoryStorageSize, int carStorageSize) {
        bodyStorage = new Storage<>(bodyStorageSize);
        bodyStorage.addSubscriber(this);

        engineStorage = new Storage<>(engineStorageSize);
        engineStorage.addSubscriber(this);

        accessoryStorage = new Storage<>(accessoryStorageSize);
        accessoryStorage.addSubscriber(this);

        carStorage = new Storage<>(carStorageSize);
        carStorage.addSubscriber(this);
    }

    @Override
    public void reactOnNotify() {
        publishNotify();
    }

    public Storage<Body> getBodyStorage() {
        return bodyStorage;
    }

    public Storage<Engine> getEngineStorage() {
        return engineStorage;
    }

    public Storage<Accessory> getAccessoryStorage() {
        return accessoryStorage;
    }

    public Storage<Car> getCarStorage() {
        return carStorage;
    }
}