package org.mikhail.factory.model.employees.unions;

import org.mikhail.factory.model.employees.Supplier;
import org.mikhail.factory.model.productsFactory.ProductCreator;
import org.mikhail.factory.model.productsFactory.products.Accessory;
import org.mikhail.factory.model.storage.StorageManager;

import java.util.ArrayList;
import java.util.List;

public class AccessorySuppliersUnion {
    private final List<Supplier<Accessory>> accessorySuppliersUnion = new ArrayList<>();

    public AccessorySuppliersUnion(
            int accessorySuppliersCount,
            long delay,
            StorageManager storageManager,
            ProductCreator<Accessory> accessoryCreator
    ) {
        inviteAccessorySuppliers(accessorySuppliersCount, delay, storageManager, accessoryCreator);
    }

    public List<Supplier<Accessory>> getAccessorySuppliersUnion() {
        return accessorySuppliersUnion;
    }

    public void setDelay(long delay) {
        for (Supplier<Accessory> accessorySupplier : accessorySuppliersUnion) {
            accessorySupplier.setDelay(delay);
        }
    }

    private void inviteAccessorySuppliers(
            int accessorySuppliersCount,
            long delay,
            StorageManager storageManager,
            ProductCreator<Accessory> accessoryCreator
    ) {
        for (int i = 0; i < accessorySuppliersCount; ++i) {
            accessorySuppliersUnion.add(new Supplier<>(
                    "accessorySupplier " + i,
                    storageManager.getAccessoryStorage(),
                    accessoryCreator,
                    delay
            ));
        }
    }
}
