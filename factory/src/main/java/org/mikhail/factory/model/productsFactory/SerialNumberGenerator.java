package org.mikhail.factory.model.productsFactory;

public class SerialNumberGenerator {
    private long lastSerialNumber = 0;

    public long generate() {
        return ++lastSerialNumber;
    }
}
