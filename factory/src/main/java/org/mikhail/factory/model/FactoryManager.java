package org.mikhail.factory.model;

import org.mikhail.factory.model.employees.EmployeesManager;
import org.mikhail.factory.model.storage.StorageManager;

public class FactoryManager {
    private final StorageManager storageManager;
    private final EmployeesManager employeesManager;

    public FactoryManager(StorageManager storageManager, EmployeesManager employeesManager) {
        this.storageManager = storageManager;
        this.employeesManager = employeesManager;
    }

    public StorageManager getStorageManager() {
        return storageManager;
    }

    public EmployeesManager getEmployeesManager() {
        return employeesManager;
    }
}
