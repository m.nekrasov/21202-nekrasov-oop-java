package org.mikhail.factory.model.productsFactory;

import org.mikhail.factory.model.productsFactory.products.Engine;
import org.mikhail.factory.model.productsFactory.products.Product;

public class EngineCreator extends ProductCreator<Engine> {
    public EngineCreator() {
        super(new SerialNumberGenerator());
    }

    @Override
    public Engine createProduct(Product... products) {
        return new Engine(serialNumberGenerator.generate());
    }
}