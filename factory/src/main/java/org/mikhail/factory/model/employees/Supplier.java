package org.mikhail.factory.model.employees;

import org.mikhail.factory.model.productsFactory.ProductCreator;
import org.mikhail.factory.model.productsFactory.products.Product;
import org.mikhail.factory.model.storage.Storage;
import org.mikhail.factory.threadpool.ITask;

public class Supplier<ProductType extends Product> implements ITask {
    private final String name;
    private final Storage<ProductType> storage;
    private final ProductCreator<ProductType> productCreator;
    private long delay;

    public Supplier(String name, Storage<ProductType> storage, ProductCreator<ProductType> productCreator, long delay) {
        this.name = name;
        this.storage = storage;
        this.productCreator = productCreator;
        this.delay = delay;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void performWork() throws InterruptedException {
        while (true) {
            try {
                Thread.sleep(delay);
                storage.put(productCreator.createProduct());
            } catch (InterruptedException exception) {
                break;
            }
        }
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }
}
