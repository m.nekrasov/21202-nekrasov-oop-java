package org.mikhail.factory.model.productsFactory;

import org.mikhail.factory.model.productsFactory.products.Accessory;
import org.mikhail.factory.model.productsFactory.products.Body;
import org.mikhail.factory.model.productsFactory.products.Car;
import org.mikhail.factory.model.productsFactory.products.Engine;
import org.mikhail.factory.model.productsFactory.products.Product;

public class CarCreator extends ProductCreator<Car> {
    public CarCreator() {
        super(new SerialNumberGenerator());
    }

    @Override
    public Car createProduct(Product... products) {
        Product body = null;
        Product engine = null;
        Product accessory = null;

        for (Product product : products) {
            if (product.getClass() == Body.class) {
                body = product;
            } else if (product.getClass() == Engine.class) {
                engine = product;
            } else if (product.getClass() == Accessory.class) {
                accessory = product;
            }
        }

        return new Car(serialNumberGenerator.generate(), body, engine, accessory);
    }
}