package org.mikhail.factory.model.productsFactory;

import org.mikhail.factory.model.productsFactory.products.Accessory;
import org.mikhail.factory.model.productsFactory.products.Product;

public class AccessoryCreator extends ProductCreator<Accessory> {
    public AccessoryCreator() {
        super(new SerialNumberGenerator());
    }

    @Override
    public Accessory createProduct(Product... products) {
        return new Accessory(serialNumberGenerator.generate());
    }
}
