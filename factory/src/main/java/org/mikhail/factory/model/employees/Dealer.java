package org.mikhail.factory.model.employees;

import org.mikhail.factory.model.productsFactory.products.Car;
import org.mikhail.factory.model.storage.StorageManager;
import org.mikhail.factory.threadpool.ITask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class Dealer implements ITask {
    private static final Logger logger = LoggerFactory.getLogger(Dealer.class);
    private final String name;
    private final StorageManager storageManager;
    private long delay;

    public Dealer(String name, StorageManager storageManager, long delay) {
        this.name = name;
        this.storageManager = storageManager;
        this.delay = delay;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void performWork() throws InterruptedException {
        while (true) {
            try {
                Thread.sleep(delay);
                Car car = storageManager.getCarStorage().get();

                logger.info(
                        LocalDateTime.now() + " | "
                        + name + ": Auto " + car.getId()
                        + " (Body: " + car.getBody().getId() + ", "
                        + "engine: " + car.getEngine().getId() + ", "
                        + "accessory: " + car.getAccessory().getId() + ")"
                );
            } catch (InterruptedException exception) {
                break;
            }
        }
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }
}
