package org.mikhail.factory.model.productsFactory;

import org.mikhail.factory.model.productsFactory.products.Body;
import org.mikhail.factory.model.productsFactory.products.Product;

public class BodyCreator extends ProductCreator<Body> {
    public BodyCreator() {
        super(new SerialNumberGenerator());
    }

    @Override
    public Body createProduct(Product... products) {
        return new Body(serialNumberGenerator.generate());
    }
}
