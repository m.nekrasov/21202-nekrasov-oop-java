package org.mikhail.factory.model.employees;

import org.mikhail.factory.model.employees.unions.AccessorySuppliersUnion;
import org.mikhail.factory.model.employees.unions.DealersUnion;
import org.mikhail.factory.model.employees.unions.WorkersUnion;
import org.mikhail.factory.model.productsFactory.products.Body;
import org.mikhail.factory.model.productsFactory.products.Engine;
import org.mikhail.factory.threadpool.ITask;

import java.util.ArrayList;
import java.util.List;

public class EmployeesManager {
    private final AccessorySuppliersUnion accessorySuppliersUnion;
    private final DealersUnion dealersUnion;
    private final WorkersUnion workersUnion;

    private final Supplier<Body> bodySupplier;
    private final Supplier<Engine> engineSupplier;

    private List<ITask> employees;

    public EmployeesManager(
            AccessorySuppliersUnion accessorySuppliersUnion,
            DealersUnion dealersUnion,
            WorkersUnion workersUnion,
            Supplier<Body> bodySupplier,
            Supplier<Engine> engineSupplier
    ) {
        this.accessorySuppliersUnion = accessorySuppliersUnion;
        this.dealersUnion = dealersUnion;
        this.workersUnion = workersUnion;
        this.bodySupplier = bodySupplier;
        this.engineSupplier = engineSupplier;

        createEmployeesList();
    }

    public void setAccessorySuppliersUnionDelay(long delay) {
        accessorySuppliersUnion.setDelay(delay);
    }

    public void setDealersUnionDelay(long delay) {
        dealersUnion.setDelay(delay);
    }

    public void setBodySupplierDelay(long delay) {
        bodySupplier.setDelay(delay);
    }

    public void setEngineSupplierDelay(long delay) {
        engineSupplier.setDelay(delay);
    }

    public List<ITask> getEmployees() {
        return employees;
    }

    public int getEmployeesCount() {
        return employees.size();
    }

    private void createEmployeesList() {
        employees = new ArrayList<>();

        employees.add(bodySupplier);
        employees.add(engineSupplier);

        employees.addAll(accessorySuppliersUnion.getAccessorySuppliersUnion());
        employees.addAll(dealersUnion.getDealersUnion());
        employees.addAll(workersUnion.getWorkersUnion());
    }
}
