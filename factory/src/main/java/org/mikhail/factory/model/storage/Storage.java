package org.mikhail.factory.model.storage;

import org.mikhail.factory.model.productsFactory.products.Product;
import org.mikhail.factory.tools.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;

public class Storage<ProductType extends Product> extends Publisher {
    private static final Logger logger = LoggerFactory.getLogger(Storage.class);
    private final Queue<ProductType> products = new LinkedList<>();
    private final long maxProducts;
    private long produced;

    public Storage(long maxProducts) {
        this.maxProducts = maxProducts;
        produced = 0;
    }

    public long getProduced() {
        return produced;
    }

    public synchronized void put(ProductType product) {
        while (true) {
            if (isFull()) {
                try {
                    wait();
                } catch (InterruptedException exception) {
                    logger.warn(exception.getMessage(), exception.getCause());
                }
            } else {
                break;
            }
        }

        products.add(product);
        ++produced;
        publishNotify();
        notify();
    }

    public synchronized ProductType get() {
        while (true) {
            if (isEmpty()) {
                try {
                    wait();
                } catch (InterruptedException exception) {
                    logger.warn(exception.getMessage(), exception.getCause());
                }
            } else {
                break;
            }
        }

        ProductType product = products.poll();
        publishNotify();
        notify();

        return product;
    }

    public long getCurrentWorkload() {
        return products.size();
    }

    public boolean isEmpty() {
        return products.isEmpty();
    }

    public boolean isFull() {
        return products.size() == maxProducts;
    }
}