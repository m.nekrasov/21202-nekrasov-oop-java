package org.mikhail.factory.model.employees;

import org.mikhail.factory.model.productsFactory.CarCreator;
import org.mikhail.factory.model.productsFactory.products.Accessory;
import org.mikhail.factory.model.productsFactory.products.Body;
import org.mikhail.factory.model.productsFactory.products.Car;
import org.mikhail.factory.model.productsFactory.products.Engine;
import org.mikhail.factory.model.storage.StorageManager;
import org.mikhail.factory.threadpool.ITask;

public class Worker implements ITask {
    private final String name;
    private final StorageManager storageManager;
    private final CarCreator carCreator;

    public Worker(String name, StorageManager storageManager, CarCreator carCreator) {
        this.name = name;
        this.storageManager = storageManager;
        this.carCreator = carCreator;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void performWork() throws InterruptedException {
        while (true) {
            Body body = storageManager.getBodyStorage().get();
            Engine engine = storageManager.getEngineStorage().get();
            Accessory accessory = storageManager.getAccessoryStorage().get();

            Car car = carCreator.createProduct(body, engine, accessory);
            storageManager.getCarStorage().put(car);
        }
    }
}