package org.mikhail.factory.model;

import org.mikhail.factory.exceptions.FactoryConfigurationException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FactoryConfiguration {
    private final Properties properties = new Properties();

    public FactoryConfiguration() throws FactoryConfigurationException {
        try (InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream("factoryConfiguration.properties")) {
            properties.load(inputStream);
        } catch (IOException exception) {
            throw new FactoryConfigurationException("Failed to load factory configuration", exception);
        }
    }

    public int getBodyStorageSize() {
        return Integer.parseInt(properties.getProperty("bodyStorageSize"));
    }

    public int getEngineStorageSize() {
        return Integer.parseInt(properties.getProperty("engineStorageSize"));
    }

    public int getAccessoryStorageSize() {
        return Integer.parseInt(properties.getProperty("accessoryStorageSize"));
    }

    public int getCarStorageSize() {
        return Integer.parseInt(properties.getProperty("carStorageSize"));
    }

    public int getAccessorySuppliers() {
        return Integer.parseInt(properties.getProperty("accessorySuppliers"));
    }

    public int getWorkers() {
        return Integer.parseInt(properties.getProperty("workers"));
    }

    public int getDealers() {
        return Integer.parseInt(properties.getProperty("dealers"));
    }
}