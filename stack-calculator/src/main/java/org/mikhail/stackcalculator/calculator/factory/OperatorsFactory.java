package org.mikhail.stackcalculator.calculator.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.operators.IOperator;
import org.mikhail.stackcalculator.exceptions.FactoryException;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class OperatorsFactory {
    private static final Logger logger = LogManager.getLogger();
    private static OperatorsFactory instance;
    private final Map<String, String> operatorNamesToClassNames = new TreeMap<>();

    private OperatorsFactory() throws FactoryException {
        PropertiesManager propertiesManager = new PropertiesManager();
        try {
            propertiesManager.loadProperties();
            propertiesManager.saveProperties(operatorNamesToClassNames);
        } catch (IOException exception) {
            throw new FactoryException("Factory cannot be initialized", exception);
        }
    }

    public static OperatorsFactory getInstance() throws FactoryException {
        if (instance == null) {
            instance = new OperatorsFactory();
        }
        return instance;
    }

    public IOperator getOperatorInstance(String operatorName) throws FactoryException {
        String className = operatorNamesToClassNames.get(operatorName);
        logger.debug("Class name for " + operatorName + ": " + className);

        if (className == null) {
            throw new FactoryException("Invalid operator name: " + operatorName);
        }

        IOperator operator;
        try {
            Class<?> operatorClass = Class.forName(className);
            logger.debug("Class for " + operatorName + ": " + operatorClass);

            operator = (IOperator)operatorClass.getDeclaredConstructor().newInstance();
            logger.debug("Operator for " + operatorName + ": " + operator);
        } catch (ClassNotFoundException exception) {
            throw new FactoryException("Impossible to find class " + className, exception);
        } catch (ReflectiveOperationException exception) {
            throw new FactoryException("Impossible to find constructor for class " + className, exception);
        }

        return operator;
    }
}
