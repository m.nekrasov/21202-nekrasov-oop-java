package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.calculator.common.CalculatorToken;
import org.mikhail.stackcalculator.exceptions.OperandsCountException;
import org.mikhail.stackcalculator.exceptions.OperandsInterpretationException;
import org.mikhail.stackcalculator.exceptions.VariablesException;

public class Define extends OperatorWithOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws OperandsCountException, VariablesException, OperandsInterpretationException {
        if (operands.size() != 2) {
            throw new OperandsCountException(2, operands);
        }

        CalculatorToken identifier = operands.get(0);
        if (!identifier.isIdentifier()) {
            throw new VariablesException("Invalid variable name: " + identifier);
        }

        Double value = operands.get(1).toDouble();
        logger.debug(identifier + "(" + value + ") will be saved in the map");

        executionContext.getVariables().put(identifier.toString(), value);
        logger.debug(identifier + "(" + value + ") save in the map");
        logger.debug("Actual map size: " + executionContext.getVariables().size());
        logger.debug("Actual map content: " + executionContext.getVariables());
    }
}
