package org.mikhail.stackcalculator.calculator.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class PropertiesManager {
    private static final Logger logger = LogManager.getLogger();
    private final Properties properties = new Properties();

    public void loadProperties() throws IOException {
        String configFileName = "config.properties";
        InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream(configFileName);
        properties.load(inputStream);
        logger.debug("Properties from " + configFileName + ": " + properties);
    }

    public void saveProperties(Map<String, String> dstMap) {
        properties.forEach((key, value) -> dstMap.put(key.toString(), value.toString()));
        logger.debug("Map of properties: " + dstMap);
    }

    public Properties getProperties() {
        return properties;
    }
}
