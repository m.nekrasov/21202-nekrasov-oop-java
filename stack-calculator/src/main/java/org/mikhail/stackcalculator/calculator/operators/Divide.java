package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.exceptions.CalculationException;
import org.mikhail.stackcalculator.exceptions.StackSizeException;

import java.util.Stack;

public class Divide extends OperatorWithoutOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws StackSizeException, CalculationException {
        Stack<Double> stack = executionContext.getStack();
        if (stack.size() < 2) {
            throw new StackSizeException(2, stack);
        }

        Double divisible = stack.pop();
        logger.debug("The divisible value removed from the stack: " + divisible);
        Double divisor = stack.pop();
        logger.debug("The divisor removed from the stack: " + divisor);

        Double result = divisible / divisor;
        if (result.isInfinite() || result.isNaN()) {
            throw new CalculationException("Impossible to divide", divisible, divisor);
        }
        logger.debug(divisible + " / " + divisor + " = " + result);

        stack.push(result);
        logger.debug(result + " pushed to the stack");
        logger.debug("Actual stack size: " + stack.size());
        logger.debug("Actual stack content: " + stack);
    }
}
