package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.exceptions.StackSizeException;

import java.util.Stack;

public class Add extends OperatorWithoutOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws StackSizeException {
        Stack<Double> stack = executionContext.getStack();
        if (stack.size() < 2) {
            throw new StackSizeException(2, stack);
        }

        Double firstSummand = stack.pop();
        logger.debug("The first summand removed from the stack: " + firstSummand);
        Double secondSummand = stack.pop();
        logger.debug("The second summand removed from the stack: " + secondSummand);

        Double result = firstSummand + secondSummand;
        logger.debug(firstSummand + " + " + secondSummand + " = " + result);

        stack.push(result);
        logger.debug(result + " pushed to the stack");
        logger.debug("Actual stack size: " + stack.size());
        logger.debug("Actual stack content: " + stack);
    }
}
