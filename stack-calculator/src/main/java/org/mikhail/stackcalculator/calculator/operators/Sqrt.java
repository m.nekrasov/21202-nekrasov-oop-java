package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.exceptions.CalculationException;
import org.mikhail.stackcalculator.exceptions.StackSizeException;

import java.util.Stack;

public class Sqrt extends OperatorWithoutOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws StackSizeException, CalculationException {
        Stack<Double> stack = executionContext.getStack();
        if (stack.isEmpty()) {
            throw new StackSizeException(1, stack);
        }

        Double value = stack.pop();
        logger.debug(value + " removed from the stack");

        Double result = Math.sqrt(value);
        if (result.isInfinite() || result.isNaN()) {
            throw new CalculationException("Impossible to extract the square root", value);
        }
        logger.debug("sqrt(" + value + ") = " + result);

        stack.push(result);
        logger.debug(result + " pushed to the stack");
        logger.debug("Actual stack size: " + stack.size());
        logger.debug("Actual stack content: " + stack);
    }
}
