package org.mikhail.stackcalculator.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.common.CalculatorOperation;
import org.mikhail.stackcalculator.calculator.factory.OperatorsFactory;
import org.mikhail.stackcalculator.calculator.operators.IOperator;
import org.mikhail.stackcalculator.exceptions.CalculationException;
import org.mikhail.stackcalculator.exceptions.FactoryException;
import org.mikhail.stackcalculator.exceptions.OperandsCountException;
import org.mikhail.stackcalculator.exceptions.OperandsInterpretationException;
import org.mikhail.stackcalculator.exceptions.StackSizeException;
import org.mikhail.stackcalculator.exceptions.VariablesException;

public class OperationsExecutor {
    private static final Logger logger = LogManager.getLogger();
    private final OperatorsFactory operatorsFactory;
    private final ExecutionContext executionContext = new ExecutionContext();

    public OperationsExecutor() throws FactoryException {
        operatorsFactory = OperatorsFactory.getInstance();
    }

    public void execute(CalculatorOperation calculatorOperation) throws
            FactoryException,
            OperandsInterpretationException,
            CalculationException,
            OperandsCountException,
            VariablesException,
            StackSizeException {
        IOperator operator = operatorsFactory.getOperatorInstance(calculatorOperation.getOperatorName());
        operator.saveOperands(calculatorOperation.getOperands());
        operator.execute(executionContext);

        String operatorOutput = operator.getOutput();
        if (operatorOutput != null) {
            logger.info(operatorOutput);
        }
    }
}
