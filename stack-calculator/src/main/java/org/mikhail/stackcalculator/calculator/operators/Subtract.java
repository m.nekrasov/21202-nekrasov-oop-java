package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.exceptions.StackSizeException;

import java.util.Stack;

public class Subtract extends OperatorWithoutOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws StackSizeException {
        Stack<Double> stack = executionContext.getStack();
        if (stack.size() < 2) {
            throw new StackSizeException(2, stack);
        }

        Double reduced = stack.pop();
        logger.debug("The reduced value removed from the stack: " + reduced);
        Double subtracted = stack.pop();
        logger.debug("The subtracted value removed from the stack: " + subtracted);

        Double result = reduced - subtracted;
        logger.debug(reduced + " - " + subtracted + " = " + result);

        stack.push(result);
        logger.debug(reduced + " pushed to the stack");
        logger.debug("Actual stack size: " + stack.size());
        logger.debug("Actual stack content: " + stack);
    }
}
