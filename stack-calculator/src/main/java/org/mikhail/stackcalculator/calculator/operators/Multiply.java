package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.exceptions.StackSizeException;

import java.util.Stack;

public class Multiply extends OperatorWithoutOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws StackSizeException {
        Stack<Double> stack = executionContext.getStack();
        if (stack.size() < 2) {
            throw new StackSizeException(2, stack);
        }

        Double firstMultiplier = stack.pop();
        logger.debug("The first multiplier removed from the stack: " + firstMultiplier);
        Double secondMultiplier = stack.pop();
        logger.debug("The second multiplier removed from the stack: " + secondMultiplier);

        Double result = firstMultiplier * secondMultiplier;
        logger.debug(firstMultiplier + " * " + secondMultiplier + " = " + result);

        stack.push(result);
        logger.debug(result + " pushed to the stack");
        logger.debug("Actual stack size: " + stack.size());
        logger.debug("Actual stack content: " + stack);
    }
}
