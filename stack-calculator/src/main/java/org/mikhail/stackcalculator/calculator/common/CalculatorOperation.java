package org.mikhail.stackcalculator.calculator.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.exceptions.NoTokensException;

import java.util.List;

public class CalculatorOperation {
    private static final Logger logger = LogManager.getLogger();
    private final String operatorName;
    private final List<CalculatorToken> operands;

    public CalculatorOperation(List<CalculatorToken> lineTokens) {
        if (lineTokens == null || lineTokens.isEmpty()) {
            throw new NoTokensException("Invalid line tokens list: " + lineTokens);
        }

        operatorName = lineTokens.get(0).toString().toUpperCase();
        logger.debug("Operator name: " + operatorName);

        operands = lineTokens.subList(1, lineTokens.size());
        logger.debug("Operands: " + operands);
    }

    public String getOperatorName() {
        return operatorName;
    }

    public List<CalculatorToken> getOperands() {
        return operands;
    }
}
