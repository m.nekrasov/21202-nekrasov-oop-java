package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.calculator.common.CalculatorToken;
import org.mikhail.stackcalculator.exceptions.OperandsCountException;
import org.mikhail.stackcalculator.exceptions.OperandsInterpretationException;
import org.mikhail.stackcalculator.exceptions.VariablesException;

public class Push extends OperatorWithOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws OperandsCountException, VariablesException, OperandsInterpretationException {
        if (operands.size() != 1) {
            throw new OperandsCountException(1, operands);
        }

        CalculatorToken calculatorToken = operands.get(0);
        Double newValue;

        if (calculatorToken.isIdentifier()) {
            String identifier = calculatorToken.toString();
            newValue = executionContext.getVariables().get(identifier);
            if (newValue == null) {
                throw new VariablesException("There is no variable \"" + identifier + "\"");
            }

            logger.debug(identifier + " (" + newValue + ") will be pushed to the stack");
        } else {
            newValue = calculatorToken.toDouble();
            logger.debug(newValue + " will be pushed to the stack");
        }

        executionContext.getStack().push(newValue);
        logger.debug(newValue + " pushed to the stack");
        logger.debug("Actual stack size: " + executionContext.getStack().size());
        logger.debug("Actual stack content: " + executionContext.getStack());
    }
}
