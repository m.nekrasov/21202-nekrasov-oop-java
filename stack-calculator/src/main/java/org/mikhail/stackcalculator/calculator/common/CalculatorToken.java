package org.mikhail.stackcalculator.calculator.common;

import org.mikhail.stackcalculator.exceptions.OperandsInterpretationException;

import javax.lang.model.SourceVersion;

public class CalculatorToken {
    private String stringValue;
    private Double doubleValue;

    public CalculatorToken(String stringValue) {
        this.stringValue = stringValue;
    }

    public CalculatorToken(Double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public Double toDouble() throws OperandsInterpretationException {
        if (doubleValue == null) {
            try {
                doubleValue = Double.valueOf(stringValue);
            } catch (NumberFormatException exception) {
                throw new OperandsInterpretationException(this);
            }
        }
        return doubleValue;
    }

    public boolean isIdentifier() {
        return SourceVersion.isIdentifier(toString());
    }

    @Override
    public String toString() {
        if (stringValue == null) {
            stringValue = doubleValue.toString();
        }
        return stringValue;
    }
}
