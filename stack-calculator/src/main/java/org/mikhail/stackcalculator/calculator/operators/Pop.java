package org.mikhail.stackcalculator.calculator.operators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.exceptions.StackSizeException;

import java.util.Stack;

public class Pop extends OperatorWithoutOperands {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(ExecutionContext executionContext) throws StackSizeException {
        Stack<Double> stack = executionContext.getStack();
        if (stack.isEmpty()) {
            throw new StackSizeException(1, stack);
        }

        Double valueToDelete = stack.pop();
        logger.debug(valueToDelete + " removed from the stack");
        logger.debug("Actual stack size: " + stack.size());
        logger.debug("Actual stack content: " + stack);
    }
}
