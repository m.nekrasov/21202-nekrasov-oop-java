package org.mikhail.stackcalculator.calculator.operators;

import org.mikhail.stackcalculator.calculator.ExecutionContext;
import org.mikhail.stackcalculator.calculator.common.CalculatorToken;
import org.mikhail.stackcalculator.exceptions.CalculationException;
import org.mikhail.stackcalculator.exceptions.OperandsCountException;
import org.mikhail.stackcalculator.exceptions.OperandsInterpretationException;
import org.mikhail.stackcalculator.exceptions.StackSizeException;
import org.mikhail.stackcalculator.exceptions.VariablesException;

import java.util.List;

public interface IOperator {
    void saveOperands(List<CalculatorToken> operands);

    void execute(ExecutionContext executionContext) throws
            OperandsCountException,
            VariablesException,
            OperandsInterpretationException,
            StackSizeException,
            CalculationException;

    String getOutput();
}
