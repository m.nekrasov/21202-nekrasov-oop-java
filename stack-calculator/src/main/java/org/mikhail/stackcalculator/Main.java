package org.mikhail.stackcalculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.OperationsExecutor;
import org.mikhail.stackcalculator.calculator.common.CalculatorOperation;
import org.mikhail.stackcalculator.calculator.common.CalculatorToken;
import org.mikhail.stackcalculator.exceptions.CalculationException;
import org.mikhail.stackcalculator.exceptions.FactoryException;
import org.mikhail.stackcalculator.exceptions.InputStreamException;
import org.mikhail.stackcalculator.exceptions.OperandsCountException;
import org.mikhail.stackcalculator.exceptions.OperandsInterpretationException;
import org.mikhail.stackcalculator.exceptions.StackSizeException;
import org.mikhail.stackcalculator.exceptions.VariablesException;
import org.mikhail.stackcalculator.parsing.InputStreamManager;
import org.mikhail.stackcalculator.parsing.Line;
import org.mikhail.stackcalculator.parsing.ReaderManager;
import org.mikhail.stackcalculator.parsing.TokensParser;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

public class Main {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        try (Reader reader = ReaderManager.getReader(InputStreamManager.getInputFileName(args))) {
            OperationsExecutor operationsExecutor;
            try {
                operationsExecutor = new OperationsExecutor();
            } catch (FactoryException exception) {
                logger.error("Impossible to initialize operations executor", exception);
                return;
            }
            Line line = new Line(reader);

            while (true) {
                line.readLine();
                if (line.endOfFileReached()) {
                    break;
                } else if (line.isEmpty()) {
                    continue;
                }

                List<CalculatorToken> lineTokens = TokensParser.getTokens(line);
                if (lineTokens == null) {
                    continue;
                }

                CalculatorOperation calculatorOperation = new CalculatorOperation(lineTokens);
                try {
                    operationsExecutor.execute(calculatorOperation);
                } catch (FactoryException
                         | OperandsCountException
                         | VariablesException
                         | OperandsInterpretationException
                         | StackSizeException
                         | CalculationException exception) {
                    logger.warn("Impossible to execute operation", exception);
                }
            }
        } catch (IOException | InputStreamException exception) {
            logger.error("Impossible to launch calculator", exception);
        }
    }
}
