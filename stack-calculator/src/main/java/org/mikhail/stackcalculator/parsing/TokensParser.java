package org.mikhail.stackcalculator.parsing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.calculator.common.CalculatorToken;
import org.mikhail.stackcalculator.exceptions.EmptyLineException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class TokensParser {
    private static final Logger logger = LogManager.getLogger();

    public static List<CalculatorToken> getTokens(Line line) throws IOException {
        if (line.isNull() || line.isEmpty()) {
            throw new EmptyLineException("Invalid line: " + line);
        }

        List<CalculatorToken> lineTokens = new ArrayList<>();

        Reader reader = new BufferedReader(new StringReader(line.toString()));
        StreamTokenizer streamTokenizer = new StreamTokenizer(reader);
        streamTokenizer.ordinaryChar('-'); // Don't parse minus as part of numbers
        streamTokenizer.wordChars('*', '/');
        streamTokenizer.commentChar('#');

        int newToken = streamTokenizer.nextToken();
        logger.debug("Type of new token: " + newToken);

        while (newToken != StreamTokenizer.TT_EOF) {
            if (streamTokenizer.ttype == StreamTokenizer.TT_NUMBER) {
                lineTokens.add(new CalculatorToken(streamTokenizer.nval));
                logger.debug("Number value of new token: " + streamTokenizer.nval);
            } else if (streamTokenizer.ttype == StreamTokenizer.TT_WORD) {
                lineTokens.add(new CalculatorToken(streamTokenizer.sval));
                logger.debug("String value of new token: " + streamTokenizer.sval);
            } else {
                logger.warn("Invalid type of token");
            }

            newToken = streamTokenizer.nextToken();
            logger.debug("Type of new token: " + newToken);
        }

        if (lineTokens.isEmpty()) {
            logger.debug("The list of tokens is empty");
            return null;
        }

        logger.debug("Tokens of new line: " + lineTokens);

        return lineTokens;
    }
}
