package org.mikhail.stackcalculator.parsing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class Line {
    private static final Logger logger = LogManager.getLogger();
    private final BufferedReader bufferedReader;
    private String lineContent;

    public Line(Reader reader) {
        bufferedReader = new BufferedReader(reader);
    }

    public void readLine() throws IOException {
        lineContent = bufferedReader.readLine();
        logger.debug("New line: " + lineContent);
    }

    public boolean endOfFileReached() {
        return lineContent == null;
    }

    public boolean isNull() {
        return lineContent == null;
    }

    public boolean isEmpty() {
        return lineContent.isEmpty();
    }

    @Override
    public String toString() {
        return lineContent;
    }
}
