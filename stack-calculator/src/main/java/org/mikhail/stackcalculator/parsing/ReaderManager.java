package org.mikhail.stackcalculator.parsing;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;

public class ReaderManager {
    public static Reader getReader(String inputFileName) throws FileNotFoundException {
        return inputFileName == null ? new InputStreamReader(System.in) : new FileReader(inputFileName);
    }
}
