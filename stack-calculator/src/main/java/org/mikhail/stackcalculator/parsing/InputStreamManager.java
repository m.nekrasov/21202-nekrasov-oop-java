package org.mikhail.stackcalculator.parsing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mikhail.stackcalculator.exceptions.InputStreamException;

public class InputStreamManager {
    private static final Logger logger = LogManager.getLogger();

    public static String getInputFileName(String[] args) throws InputStreamException {
        if (args == null) {
            throw new InputStreamException("Argument is null");
        }

        if (args.length == 0) {
            logger.debug("Calculator has launched in console mode");
            return null;
        } else if (args.length == 1) {
            logger.debug("Calculator has launched in text file mode");
            return args[0];
        } else {
            throw new InputStreamException("""
                    Too many arguments
                    To launch calculator in console mode run in without arguments
                    To launch calculator in text file mode run it with only one argument (input file name)
                    """);
        }
    }
}
