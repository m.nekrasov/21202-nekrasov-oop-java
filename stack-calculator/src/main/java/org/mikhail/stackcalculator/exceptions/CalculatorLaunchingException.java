package org.mikhail.stackcalculator.exceptions;

public class CalculatorLaunchingException extends CalculatorException {
    public CalculatorLaunchingException(String message) {
        super(message);
    }
}
