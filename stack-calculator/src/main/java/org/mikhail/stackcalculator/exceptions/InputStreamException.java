package org.mikhail.stackcalculator.exceptions;

public class InputStreamException extends CalculatorLaunchingException{
    public InputStreamException(String message) {
        super(message);
    }
}
