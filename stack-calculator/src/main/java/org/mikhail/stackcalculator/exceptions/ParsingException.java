package org.mikhail.stackcalculator.exceptions;

public class ParsingException extends RuntimeException {
    public ParsingException(String message) {
        super(message);
    }
}
