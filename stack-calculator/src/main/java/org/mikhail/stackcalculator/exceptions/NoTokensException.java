package org.mikhail.stackcalculator.exceptions;

public class NoTokensException extends ParsingException {
    public NoTokensException(String message) {
        super(message);
    }
}
