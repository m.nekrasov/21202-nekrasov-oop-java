package org.mikhail.stackcalculator.exceptions;

public class EmptyLineException extends ParsingException {
    public EmptyLineException(String message) {
        super(message);
    }
}
