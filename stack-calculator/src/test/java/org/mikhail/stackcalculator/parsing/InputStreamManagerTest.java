package org.mikhail.stackcalculator.parsing;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mikhail.stackcalculator.exceptions.InputStreamException;

import static org.junit.jupiter.api.Assertions.*;

public class InputStreamManagerTest {
    @Test
    @DisplayName("Test empty array")
    public void testEmptyArray() {
        String[] emptyArray = new String[0];
        assertDoesNotThrow(
                () -> assertNull(InputStreamManager.getInputFileName(emptyArray))
        );
    }

    @Test
    @DisplayName("Test single-element array")
    public void testSingleElementArray() {
        String[] singleElementArray = {"inputFileExample.txt"};
        assertDoesNotThrow(
                () -> assertEquals("inputFileExample.txt", InputStreamManager.getInputFileName(singleElementArray))
        );
    }

    @Test
    @DisplayName("Test two-element array")
    public void testTwoElementArray() {
        String[] twoElementArray = {"inputFile1.txt", "inputFile2.txt"};
        assertThrows(InputStreamException.class, () -> InputStreamManager.getInputFileName(twoElementArray));
    }
}