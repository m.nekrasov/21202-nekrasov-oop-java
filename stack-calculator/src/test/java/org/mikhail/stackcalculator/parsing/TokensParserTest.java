package org.mikhail.stackcalculator.parsing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mikhail.stackcalculator.calculator.common.CalculatorToken;
import org.mikhail.stackcalculator.exceptions.EmptyLineException;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TokensParserTest {
    private static Logger logger;
    private Line line;

    @BeforeAll
    public static void setLogger() {
        System.setProperty("logback.configurationFile", "logback-test.xml");
        logger = LogManager.getLogger();
    }

    @Test
    @Order(1)
    @DisplayName("Check empty line")
    public void checkEmptyLine() {
        String emptyContent = "";
        Reader emptyContentReader = new StringReader(emptyContent);
        line = new Line(emptyContentReader);
        assertDoesNotThrow(() -> line.readLine());

        assertThrows(EmptyLineException.class, () -> TokensParser.getTokens(line));
    }

    @Test
    @Order(2)
    @DisplayName("Check comment line")
    public void checkCommentLine() {
        String commentContent = "# Comment";
        Reader commentContentReader = new StringReader(commentContent);
        line = new Line(commentContentReader);
        assertDoesNotThrow(() -> line.readLine());

        assertDoesNotThrow(() -> TokensParser.getTokens(line));
    }

    @Test
    @Order(3)
    @DisplayName("Check result of comment line")
    public void checkResultOfCommentLine() {
        String commentContent = "# Comment";
        Reader commentContentReader = new StringReader(commentContent);
        line = new Line(commentContentReader);
        assertDoesNotThrow(() -> line.readLine());

        List<CalculatorToken> lineTokens = assertDoesNotThrow(() -> TokensParser.getTokens(line));
        assertNull(lineTokens);
    }

    @Test
    @Order(4)
    @DisplayName("Check correct line")
    public void checkCorrectLine() {
        String correctContent = "define x 1";
        Reader correctContentReader = new StringReader(correctContent);
        line = new Line(correctContentReader);
        assertDoesNotThrow(() -> line.readLine());

        assertDoesNotThrow(() -> TokensParser.getTokens(line));
    }

    @Test
    @Order(5)
    @DisplayName("Checking result of correct line")
    public void checkResultOfCorrectLine() {
        if (line == null) {
            // Example of using a logger in a test
            logger.warn("Line is null");

            String correctContent = "define x 1";
            Reader correctContentReader = new StringReader(correctContent);
            line = new Line(correctContentReader);
            assertDoesNotThrow(() -> line.readLine());
        }

        List<CalculatorToken> lineTokens = assertDoesNotThrow(() -> TokensParser.getTokens(line));
        assertNotNull(lineTokens);

        assertAll(
                () -> assertDoesNotThrow(
                        () -> assertEquals(3, lineTokens.size())
                ),
                () -> assertEquals("define", lineTokens.get(0).toString()),
                () -> assertEquals("x", lineTokens.get(1).toString()),
                () -> assertDoesNotThrow(
                        () -> assertEquals(1.0, lineTokens.get(2).toDouble())
                )
        );


    }
}