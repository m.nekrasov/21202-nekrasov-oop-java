package org.mikhail.stackcalculator.parsing;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.Reader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LineTest {
    private Line line;

    @Test
    @Order(1)
    @DisplayName("Test correct content")
    public void testCorrectContent() {
        String correctContent = """
            First line
            
            Second line
            """;
        Reader correctContentReader = new StringReader(correctContent);
        line = new Line(correctContentReader);

        assertDoesNotThrow(() -> line.readLine());
        assertFalse(line.endOfFileReached());
        assertFalse(line.isNull());
        assertFalse(line.isEmpty());
        assertEquals("First line", line.toString());

        assertDoesNotThrow(() -> line.readLine());
        assertFalse(line.endOfFileReached());
        assertFalse(line.isNull());
        assertTrue(line.isEmpty());
        assertEquals("", line.toString());

        assertDoesNotThrow(() -> line.readLine());
        assertFalse(line.endOfFileReached());
        assertFalse(line.isNull());
        assertFalse(line.isEmpty());
        assertEquals("Second line", line.toString());

        assertDoesNotThrow(() -> line.readLine());
        assertTrue(line.endOfFileReached());
        assertTrue(line.isNull());
        assertThrows(NullPointerException.class, () -> line.isEmpty());
        assertNull(line.toString());
    }

    @Test
    @Order(2)
    @DisplayName("Test incorrect content")
    public void testIncorrectContent() {
        String incorrectContent = "";
        Reader incorrectContentReader = new StringReader(incorrectContent);
        line = new Line(incorrectContentReader);

        assertDoesNotThrow(() -> line.readLine());
        assertTrue(line.endOfFileReached());
        assertTrue(line.isNull());
        assertThrows(NullPointerException.class, () -> line.isEmpty());
        assertNull(line.toString());
    }
}