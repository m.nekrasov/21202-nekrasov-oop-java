package org.mikhail.stackcalculator.parsing;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

public class ReaderManagerTest {
    @Test
    @DisplayName("Test InputStreamReader")
    public void testInputStreamReader() {
        assertDoesNotThrow(
                () -> assertInstanceOf(InputStreamReader.class, ReaderManager.getReader(null))
        );
    }

    @Test
    @DisplayName("Test FileReader")
    public void testFileReader() {
        assertDoesNotThrow(
                () -> assertInstanceOf(FileReader.class, ReaderManager.getReader("inputFileExample.txt"))
        );
    }
}