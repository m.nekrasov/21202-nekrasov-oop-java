package org.mikhail.stackcalculator.calculator.factory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

public class PropertiesManagerTest {
    private static final PropertiesManager propertiesManager = new PropertiesManager();
    private static final Properties expectedProperties = new Properties();

    @BeforeAll
    public static void preparePropertiesManager() {
        assertDoesNotThrow(propertiesManager::loadProperties);
    }

    @BeforeAll
    public static void fillExpectedProperties() {
        expectedProperties.setProperty("PUSH", "org.mikhail.stackcalculator.calculator.operators.Push");
        expectedProperties.setProperty("POP", "org.mikhail.stackcalculator.calculator.operators.Pop");

        expectedProperties.setProperty("DEFINE", "org.mikhail.stackcalculator.calculator.operators.Define");

        expectedProperties.setProperty("PRINT", "org.mikhail.stackcalculator.calculator.operators.Print");

        expectedProperties.setProperty("+", "org.mikhail.stackcalculator.calculator.operators.Add");
        expectedProperties.setProperty("-", "org.mikhail.stackcalculator.calculator.operators.Subtract");
        expectedProperties.setProperty("*", "org.mikhail.stackcalculator.calculator.operators.Multiply");
        expectedProperties.setProperty("/", "org.mikhail.stackcalculator.calculator.operators.Divide");
        expectedProperties.setProperty("SQRT", "org.mikhail.stackcalculator.calculator.operators.Sqrt");
    }

    @Test
    @DisplayName("Comparison of properties")
    public void compareProperties() {
        assertEquals(expectedProperties.toString(), propertiesManager.getProperties().toString());
    }

    @Test
    @DisplayName("Comparison of property maps")
    public void comparePropertyMaps() {
        Map<String, String> expectedPropertyMap = new TreeMap<>();
        expectedProperties.forEach((key, value) -> expectedPropertyMap.put(key.toString(), value.toString()));

        Map<String, String> actualPropertyMap = new TreeMap<>();
        propertiesManager.saveProperties(actualPropertyMap);

        assertEquals(expectedPropertyMap.toString(), actualPropertyMap.toString());
    }
}