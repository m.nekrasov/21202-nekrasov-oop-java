package org.mikhail.stackcalculator.calculator.factory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mikhail.stackcalculator.calculator.operators.Add;
import org.mikhail.stackcalculator.calculator.operators.Define;
import org.mikhail.stackcalculator.calculator.operators.Divide;
import org.mikhail.stackcalculator.calculator.operators.Multiply;
import org.mikhail.stackcalculator.calculator.operators.Pop;
import org.mikhail.stackcalculator.calculator.operators.Print;
import org.mikhail.stackcalculator.calculator.operators.Push;
import org.mikhail.stackcalculator.calculator.operators.Sqrt;
import org.mikhail.stackcalculator.calculator.operators.Subtract;
import org.mikhail.stackcalculator.exceptions.FactoryException;

import static org.junit.jupiter.api.Assertions.*;

public class OperatorsFactoryTest {
    private static OperatorsFactory operatorsFactory;

    @BeforeAll
    public static void initializeOperatorsFactory() {
        operatorsFactory = assertDoesNotThrow(OperatorsFactory::getInstance);
    }

    @Test
    @DisplayName("Getting instance of PUSH operator")
    public void getInstanceOfPushOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Push.class, operatorsFactory.getOperatorInstance("PUSH"))
        );
    }

    @Test
    @DisplayName("Getting instance of POP operator")
    public void getInstanceOfPopOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Pop.class, operatorsFactory.getOperatorInstance("POP"))
        );
    }

    @Test
    @DisplayName("Getting instance of DEFINE operator")
    public void getInstanceOfDefineOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Define.class, operatorsFactory.getOperatorInstance("DEFINE"))
        );
    }

    @Test
    @DisplayName("Getting instance of PRINT operator")
    public void getInstanceOfPrintOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Print.class, operatorsFactory.getOperatorInstance("PRINT"))
        );
    }

    @Test
    @DisplayName("Getting instance of + operator")
    public void getInstanceOfAddOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Add.class, operatorsFactory.getOperatorInstance("+"))
        );
    }

    @Test
    @DisplayName("Getting instance of - operator")
    public void getInstanceOfSubtractOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Subtract.class, operatorsFactory.getOperatorInstance("-"))
        );
    }

    @Test
    @DisplayName("Getting instance of * operator")
    public void getInstanceOfMultiplyOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Multiply.class, operatorsFactory.getOperatorInstance("*"))
        );
    }

    @Test
    @DisplayName("Getting instance of / operator")
    public void getInstanceOfDivideOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Divide.class, operatorsFactory.getOperatorInstance("/"))
        );
    }

    @Test
    @DisplayName("Getting instance of SQRT operator")
    public void getInstanceOfSqrtOperator() {
        assertDoesNotThrow(
                () -> assertInstanceOf(Sqrt.class, operatorsFactory.getOperatorInstance("SQRT"))
        );
    }

    @Test
    @DisplayName("Getting instance of incorrect operator")
    public void getInstanceOfIncorrectOperator() {
        assertThrows(FactoryException.class, () -> operatorsFactory.getOperatorInstance("%"));
    }
}