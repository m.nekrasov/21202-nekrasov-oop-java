package org.mikhail.stackcalculator.calculator.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mikhail.stackcalculator.exceptions.NoTokensException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorOperationTest {
    private final CalculatorToken operatorName = new CalculatorToken("push");
    private final CalculatorToken operand = new CalculatorToken(1.0);
    private final List<CalculatorToken> calculatorTokens = new ArrayList<>();
    private CalculatorOperation calculatorOperation;

    @BeforeEach
    @Test
    @DisplayName("Creation correct operation")
    public void createCorrectOperation() {
        calculatorTokens.add(operatorName);
        calculatorTokens.add(operand);

        assertDoesNotThrow(() -> calculatorOperation = new CalculatorOperation(calculatorTokens));
    }

    @Test
    @DisplayName("Getting operator")
    public void getOperator() {
        assertNotEquals(operatorName.toString(), calculatorOperation.getOperatorName());
        assertEquals("PUSH", calculatorOperation.getOperatorName());
    }

    @Test
    @DisplayName("Getting operands")
    public void getOperands() {
        Object[] expectedOperands = calculatorTokens.subList(1, calculatorTokens.size()).toArray();
        Object[] actualOperands = calculatorOperation.getOperands().toArray();

        assertArrayEquals(expectedOperands, actualOperands);
    }

    @Test
    @DisplayName("Creating null operation")
    public void createNullOperation() {
        assertThrows(
                NoTokensException.class, () -> new CalculatorOperation(null)
        );
    }

    @Test
    @DisplayName("Creating empty operation")
    public void createEmptyOperation() {
        assertThrows(
                NoTokensException.class, () -> new CalculatorOperation(new ArrayList<>())
        );
    }
}